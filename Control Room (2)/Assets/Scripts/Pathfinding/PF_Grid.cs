﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PF_Grid : MonoBehaviour
{
    public PF_Region region;
    public List<PF_Node> nodes = new List<PF_Node>();
    public Vector3 gridSize;
    public float gridSpacing;

    public void GenerateGrid(Vector2 size, float spacing)
    {
        gridSpacing = spacing;
        gridSize = size;

        float xOffset = (size.x - spacing) / 2;
        float yOffset = (size.y - spacing) / 2;

        for (float x = -xOffset; x <= xOffset; x += spacing)
        {
            for (float y = -yOffset; y <= yOffset; y += spacing)
            {
                GameObject nodeGO = new GameObject("Node");
                nodeGO.transform.position = new Vector3(x, 0, y) + transform.position;
                nodeGO.transform.SetParent(transform);
                PF_Node node = nodeGO.AddComponent<PF_Node>();
                node.spacing = spacing;
                node.grid = this;
                nodes.Add(node);
            }
        }
    }

    public void AssignNeighbours(List<PF_Area> areas)
    {
        foreach (PF_Node node in nodes)
        {
            node.neighbours = new List<PF_Node>();
            for (float x = -gridSpacing; x <= gridSpacing; x += gridSpacing)
            {
                for (float y = -gridSpacing; y <= gridSpacing; y += gridSpacing)
                {
                    if (x == 0 && y == 0) continue;
                    PF_Node neighbour = Pathfind.GetNode(node.transform.position + new Vector3(x, 0, y), areas);
                    if (neighbour) node.neighbours.Add(neighbour);
                }
            }
        }
    }

    public void Bake()
    {
        List<PF_Node> clearNodes = new List<PF_Node>();
        foreach (PF_Node node in nodes)
        {
            node.unwalkable = false;

            //Collider[] colliders = Physics.OverlapSphere(node.transform.position, gridSpacing / 3);
            Collider[] colliders = Physics.OverlapCapsule(node.transform.position + Vector3.up, node.transform.position + Vector3.down, gridSpacing / 3);
            if (colliders.Length == 0)
                clearNodes.Add(node);

            bool doorNode = false;
            foreach (Collider collider in colliders)
            {
                if (doorNode) continue;
                if (collider.gameObject.layer == LayerMask.NameToLayer("Environment") || collider.gameObject.layer == LayerMask.NameToLayer("StaticProp"))
                    node.unwalkable = true;
                //if (!node.unwalkable)
                //{
                    //if (collider.gameObject.layer == LayerMask.NameToLayer("StaticProp"))
                        //node.blocked = true;
                //}
                BlastDoor door = collider.GetComponentInParent<BlastDoor>();
                if (door)
                {
                    doorNode = true;
                    node.unwalkable = false;
                    node.blocked = true;
                    door.nodes.Add(node);
                }
            }
        }
        for (int i = 0; i < clearNodes.Count; i++)
        {
            PF_Node node = clearNodes[i];
            nodes.Remove(node);
            DestroyImmediate(node.gameObject);
        }
    }

    public void AssignWeight()
    {        
        //foreach(PF_Node node in nodes)
        {
            //node.avoidanceWeight = 0;
        }
        foreach(PF_Node node in nodes)
        {
            if (node.unwalkable)
            {
                node.avoidanceWeight = 100;
                foreach(PF_Node neighbour in node.neighbours)
                {
                    if (neighbour.unwalkable || neighbour.blocked) continue;
                    neighbour.avoidanceWeight = 50;
                    foreach(PF_Node nextNeighbour in neighbour.neighbours)
                    {
                        if (nextNeighbour.avoidanceWeight < 25)
                            nextNeighbour.avoidanceWeight = 25;
                    }
                }
            }
            if (node.blocked)
            {
                node.avoidanceWeight = 50000;
                foreach (PF_Node neighbour in node.neighbours)
                {
                    if (neighbour.unwalkable || neighbour.blocked) continue;
                    neighbour.avoidanceWeight = 3000;
                }
            }
        }
    }

    public PF_Node GetNode(Vector3 position, Vector3 nearPosition)
    {
        float sizeX = 0.5f + gridSize.x ;
        float sizeY = 0.5f + gridSize.y ;
        if (position.x > transform.position.x + sizeX / 2 || position.x < transform.position.x - sizeX / 2 || position.z > transform.position.z + sizeY || position.z < transform.position.z - sizeY)
        {
            return null;
        }        

        position.y = transform.position.y;
        /*
        float nearestDistance = 1000;
        PF_Node nearestNode = null;
        
        foreach (PF_Node node in nodes)
        {
            float newDistance = (node.transform.position - nearPosition).magnitude;
            if (newDistance < nearestDistance)
            {
                if (node.unwalkable) continue;
                nearestNode = node;
                nearestDistance = newDistance;
            }
            if (WithinSquare(position, node.transform.position, gridSpacing))
                return node;            
        } 
        */
        foreach (PF_Node node in nodes)
        {
            if (WithinSquare(position, node.transform.position, gridSpacing))
                return node;
        }
        //if (nearestNode) return nearestNode;
        return null;
    }

    public bool WithinSquare(Vector3 position, Vector3 squarePosition, float size)
    {
        size += 0.5f;
        return (position.x < squarePosition.x + size / 2 && position.x > squarePosition.x - size / 2 && position.z < squarePosition.z + size / 2 && position.z > squarePosition.z - size / 2);
    }

    Vector3 RoundToGrid(Vector3 position)
    {
        position.y = transform.position.y;
        position.x = (Mathf.Round(position.x / gridSpacing)) * gridSpacing;
        position.z = (Mathf.Round(position.z / gridSpacing)) * gridSpacing;
        return position;
    }

    float GetDistance(Vector3 start, Vector3 end)
    {
        Vector3 heading;
        heading.x = start.x - end.x;
        heading.y = start.y - end.y;
        heading.z = start.z - end.z;

        float distanceSquared = heading.x * heading.x + heading.y * heading.y + heading.z * heading.z;

        return Mathf.Sqrt(distanceSquared);
    }   
}
