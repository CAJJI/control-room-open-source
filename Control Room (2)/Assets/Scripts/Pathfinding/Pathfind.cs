﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PFPath
{
    public List<PF_Node> nodes = new List<PF_Node>();

    public bool ContainsBlocked()
    {
        foreach(PF_Node node in nodes)
        {
            if (node.blocked) return true;
        }
        return false;
    }

    public static PFPath Copy(PFPath copy)
    {
        PFPath newPath = new PFPath();
        newPath.nodes.AddRange(copy.nodes);
        return newPath;
    }

    public void ReadjustPath(Vector3 newPosition)
    {        
        float nearestDistance = 1000000;        
        int removeAt = 0;
        for (int i = 0; i < nodes.Count; i++)
        {
            PF_Node node = nodes[i];            
            float distance = (newPosition - node.transform.position).magnitude;
            if (distance < nearestDistance)
            {                
                nearestDistance = distance;     
                removeAt = i;   
            }            
        }
        for (int i = 0; i < removeAt; i++)
        {            
            nodes.Remove(nodes[0]);         
        }
        PFPath AddPath = Copy(this);
        PFPath PrePath = Pathfind.GetPath(newPosition, AddPath.nodes[0].transform.position);
        nodes = new List<PF_Node>();
        nodes.AddRange(PrePath.nodes);
        nodes.AddRange(AddPath.nodes);

    }
}

public static class Pathfind {
    
    public static List<PF_Area> areas = new List<PF_Area>();
    public static List<PF_Node> searchedNodes = new List<PF_Node>();

    public static PFPath GetPath(Vector3 startPos, Vector3 endPos)
    {
        PF_Node start = GetNode(startPos, endPos);
        PF_Node end = GetNode(endPos, startPos);
        return GetPath(start, end);
    }

    public static PFPath GetPath(PF_Node start, PF_Node end)
    {     
        searchedNodes = new List<PF_Node>();
        //PF_Node start = GetNode(startPos, endPos);
        //PF_Node end = GetNode(endPos, startPos);

        if (!start || !end)
        {
            return null;
        }

        if (start.blocked || start.unwalkable) start = NearestAvailable(start, end.transform.position);
        if (end.blocked || end.unwalkable) end = NearestAvailable(end, start.transform.position);
        
        if (start == null || end == null) return null;        

        List<PF_Node> openList = new List<PF_Node>();
        List<PF_Node> closedList = new List<PF_Node>();

        openList.Add(start);
        int searches = 0;
        while (openList.Count > 0 && searches < 5000)
        {
            searches++;
            PF_Node currentNode = openList[0];

            foreach (PF_Node node in openList)
            {
                if (node.fCost < currentNode.fCost || node.fCost == currentNode.fCost)
                {
                    if (node.hCost < currentNode.hCost)
                        currentNode = node;                                        
                }
            }
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (currentNode == end)            
                return RetracePath(start, end);               
            
            foreach(PF_Node neighbour in currentNode.neighbours)
            {
                if (neighbour.unwalkable || closedList.Contains(neighbour))
                {
                    if (neighbour == end)
                    {
                        return RetracePath(start, currentNode);
                    }
                    continue;
                }
                
                float newCost = currentNode.gCost + GetDistance(currentNode.transform.position, neighbour.transform.position);
                if (newCost < neighbour.gCost || !openList.Contains(neighbour))
                {
                    neighbour.gCost = newCost;
                    neighbour.hCost = GetDistance(neighbour.transform.position, end.transform.position); // + neighbour.avoidanceWeight;
                    neighbour.parent = currentNode;

                    if (!openList.Contains(neighbour))
                    {                        
                        openList.Add(neighbour);
                        searchedNodes.Add(neighbour);
                    }
                }
            }
        }
        /*PF_Node nearestNode = null;
        float distance = 1000;
        foreach(PF_Node node in closedList)
        {
            if (node.fCost < distance) nearestNode = node;
        }
        return RetracePath(start,nearestNode);
        */
        return null;
    }

    public static float GetDistance(Vector3 start, Vector3 end)
    {
        float x = Mathf.Abs(start.x - end.x);
        float y = Mathf.Abs(start.z - end.z);
        if (x > y)
            return 14 * y + 10 * (x - y);
        return 14 * x + 10 * (y - x);
    }

    public static PFPath RetracePath(PF_Node start, PF_Node end)
    {        
        PFPath path = new PFPath();
        PF_Node node = end;

        while (node != start)
        {
            path.nodes.Add(node);
            node = node.parent;
        }
        path.nodes.Reverse();
        return path;
    }

    public static PF_Node NearestAvailable(PF_Node node, Vector2 nearestPosition)
    {
        return GetNearestAvailable(node, nearestPosition, new List<PF_Node>());
    }

    public static PF_Node GetNearestAvailable(PF_Node node, Vector3 nearPosition, List<PF_Node> ignoreNodes)
    {
        return GetNearestAvailable(node, nearPosition, ignoreNodes, true);
    }

    public static PF_Node GetNearestAvailable(PF_Node node, Vector3 nearPosition, List<PF_Node> ignoreNodes, bool ignoreBlocked)
    {
        float distance = 1000;
        PF_Node nearest = null;
        PF_Node current = node;
        List<PF_Node> open = new List<PF_Node>();
        List<PF_Node> closed = new List<PF_Node>();
        closed.Add(node);
        open.Add(node);
        int searches = 0;
        while (open.Count > 0 && searches < 5000)
        {
            searches++;
            current = open[0];
            open.Remove(current);
            foreach (PF_Node neighbour in current.neighbours)
            {
                if (!closed.Contains(neighbour) && !ignoreNodes.Contains(neighbour))
                {
                    open.Add(neighbour);
                    closed.Add(neighbour);
                }
            }
            foreach (PF_Node check in open)
            {

                if ((ignoreBlocked && check.blocked) || check.unwalkable) continue;
                float newDistance = (check.transform.position - nearPosition).magnitude;
                if (newDistance < distance)
                {
                    distance = newDistance;
                    nearest = check;
                }
            }
            if (nearest != null) return nearest;
        }
        return null;
    }

    public static List<PF_Node> GetNodesNear(PF_Node node, int radius)
    {
        List<PF_Node> ignore = new List<PF_Node>();
        List<PF_Node> open = new List<PF_Node>();
        List<PF_Node> options = new List<PF_Node>();
        PF_Node currentNode = node;
        open.Add(currentNode);
        ignore.Add(node);
        int inRadius = 0;

        while (inRadius < radius && open.Count > 0)
        {
            currentNode = open[0];
            if (currentNode == null) return options;
            open.Remove(currentNode);
            ignore.Add(currentNode);
            foreach (PF_Node neighbour in currentNode.neighbours)
            {
                if (ignore.Contains(neighbour) || neighbour.blocked || neighbour.unwalkable) continue;
                if (inRadius == radius - 1)
                    options.Add(neighbour);
                ignore.Add(neighbour);
                open.Add(neighbour);
            }
            inRadius++;
        }        
        return options;
    }

    public static PF_Node GetRandomNodeNear(PF_Node node, int radius)
    {
        List<PF_Node> options = GetNodesNear(node, radius);
        if (options.Count == 0) return node;
        int random = Random.Range(0, options.Count);
        return options[random];
    }
   
    public static PF_Node GetNode(Vector3 position)
    {
        return GetNode(position, position, areas);
    }

    public static PF_Node GetNode(Vector3 position, Vector3 nearPosition)
    {
        return GetNode(position, nearPosition, areas);
    }

    public static PF_Node GetNode(Vector3 position, List<PF_Area> areas)
    {
        return GetNode(position, position, areas);
    }

    public static PF_Node GetNode(Vector3 position, Vector3 nearPosition, List<PF_Area> areas)
    {
        foreach (PF_Area area in areas)
        {
            PF_Node node = area.GetNode(position, nearPosition);
            if (node != null)
                return node;
        }
        float nearestDistance = 100000;
        PF_Node nearestNode = null;

        foreach(PF_Area area in areas)
        {
            foreach(PF_Region region in area.regions)
            {
                foreach(PF_Grid grid in region.grids)
                {
                    foreach(PF_Node node in grid.nodes)
                    {
                        float distance = (node.transform.position - position).magnitude;
                        if (distance < nearestDistance)
                        {
                            nearestDistance = distance;
                            nearestNode = node;
                        }
                    }
                }
            }
        }
        return nearestNode;
        //return null;
    }
}
