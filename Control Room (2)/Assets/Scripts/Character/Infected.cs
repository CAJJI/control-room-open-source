﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Infected : Character {
    
    float audioDelay;
    float audioPitch;
    public BreakableDoor activeDoor;
    public LayerMask avoidLayerMask;
    float waitTime;

    private void Start()
    {
        audioDelay = Random.Range(0, 20);
        audioPitch = Random.Range(-0.7f, 0.7f);
        ScreamAudio();
        //setDetectionDelay = 30;
        //detectionDelay = Random.Range(0, setDetectionDelay);
        DetectionManager.AddInfected(detector);
        float multiplier = 0.5f;
        //if (GlobalManager.levelInfo.difficulty > 1) multiplier = 1;
        maxHealth = GlobalManager.GameDef.GetCharacterDef(CharacterType.Infected).health;
        maxHealth *= 0.5f + (GlobalManager.levelInfo.difficulty * multiplier);        
        health = maxHealth;
        float damage = GlobalManager.GameDef.GetCharacterDef(CharacterType.Infected).damage;
        damage *= (0.5f + (GlobalManager.levelInfo.difficulty * multiplier));
        GetComponentInChildren<Weapon>().damageInfo.damage = damage;
    }

    void Update()
    {        
        if (dead) return;

        if (audioDelay % 20 == 0)
            ScreamAudio();
        audioDelay++;
        
        ActiveTarget();
        if (PhotonNetwork.inRoom && !photonView.isMine) return;
        UpdateCharacter();

        Movement();
        Detection();
        if (health <= 0)
            Die(new DamageInfo(100, Vector3.zero, this, GetLimb(1)));
    }


    [PunRPC]
    public override void RPCDie(int characterId, float damage, Vector3 direction, int limbId)
    {
        if (dead) return;
        DamageInfo info = DamageInfo.GetInfo(this, characterId, damage, direction, limbId);
        Weapon weapon = GetComponentInChildren<Weapon>();
        if (weapon)
            Destroy(weapon.gameObject);
        base.RPCDie(characterId, damage, direction, limbId);
    }

    void Avoid()
    {
        //float hitDistance = 0;
        Vector3 direction = Vector3.zero;
        RaycastHit hit;
        if (Physics.Raycast(transform.position + (transform.forward / 2) + Vector3.up, transform.forward, out hit, 2, avoidLayerMask))
        {
            direction = transform.right;            
            Move(direction, movementSpeed / 2, false, true);
        }
        //if (Physics.Raycast(transform.position + (transform.forward/2) + Vector3.up, transform.forward, out hit, 2, avoidLayerMask))
        //{                        
        //    direction = transform.right;
        //    if (Physics.Raycast(transform.position + (transform.forward / 2) + Vector3.up, transform.forward + transform.right, out hit, 2, avoidLayerMask))
        //    {
        //        hitDistance = hit.distance;
        //        direction = -transform.right;
        //        if (Physics.Raycast(transform.position + (transform.forward / 2) + Vector3.up, transform.forward - transform.right, out hit, 2, avoidLayerMask))
        //        {
        //            if (hit.distance < hitDistance)
        //                direction = transform.right;
        //        }
        //    }
        //    Move(direction, movementSpeed/2, false, true);
        //}
    }

    void ScreamAudio()
    {
        if (Player.isDead) return;
        float distance = (transform.position - Player.Instance.transform.position).magnitude;
        float pitch = Mathf.Clamp(1.5f - (distance / 50), 0.1f, 1.5f) + audioPitch;
        audioSource.pitch = pitch;
    }

    void ActiveTarget()
    {        
        if (!activeTarget) return;

        Avoid();
        if (activeTarget == activeDoor)
        {
            if (activeDoor.door.open)
            {
                activeTarget = null;
                activeDoor = null;
            }
            else
            {
                Vector3 position = activeTarget.transform.position; position.y = transform.position.y;
                float speed = Mathf.Clamp(DistancedMovementSpeed(activeTarget.transform.position, transform.position, movementSpeed, 1), 0.001f, 0.1f);
                Move((position - transform.position), speed, true, true);
                Attack();
            }
            return;
        }
        else
            activeDoor = null;

        if (activeTarget.dead)
        {            
            activeTarget = null;
            return;
        }
        if (detector.CanSee(transform.position + Vector3.up, activeTarget.transform.position + Vector3.up, activeTarget.gameObject, sightLayerMask))
        {
            float distance = (transform.position - activeTarget.transform.position).magnitude;
            float speed = Mathf.Clamp(DistancedMovementSpeed(activeTarget.transform.position, transform.position, movementSpeed * 2, 3), 0.13f, 0.2f);
            Move((activeTarget.transform.position - transform.position), speed, true, true);
            if (activeTarget.transform.position.y > transform.position.y + 0.2f && distance < 5)
            {
                Jump(Vector3.up);
            }
            if (distance < 5)
            Attack();
        }
        else
        {
            if (activePath != null && activePath.nodes.Count > 0)
                activePath.ReadjustPath(transform.position);
            activeTarget = null;
        }
    }

    void Detection()
    {
        //detectionDelay++;
        //int setDetection = setDetectionDelay;
        //if (activeDoor) setDetection *= 2;
        //if (detectionDelay % setDetection != 0) return;        
        if (activeTarget && (activeTarget != activeDoor)) return;
        if (!detector.updated) return;

        //detector.UpdateDetections();

        Character nearest = null;
        float nearestDistance = 1000000;
        foreach (GameObject detection in detector.detections)
        {
            if (!detection) continue;
            Character character = detection.GetComponentInParent<Character>();
            if (character && !character.dead && detector.CanSee(transform.position + Vector3.up, character.transform.position + Vector3.up, character.gameObject, sightLayerMask))
            {
                float distance = (transform.position - character.transform.position).sqrMagnitude;
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearest = character;
                }                
            }
        }
        activeTarget = nearest;
    }

    void Movement()
    {
        if (activeTarget) return;
        if (activePath != null && activePath.nodes.Count > 0)
        {
            if (activePath.nodes[0].blocked)
            {
                Collider[] colliders = Physics.OverlapSphere(activePath.nodes[0].transform.position, 0.5f);
                foreach (Collider collider in colliders)
                {
                    BreakableDoor door = collider.GetComponentInParent<BreakableDoor>();
                    if (door && !door.door.open)
                    {
                        activeTarget = door;
                        activeDoor = door;
                        return;
                    }
                }
            }
            float speed = DistancedMovementSpeed(activePath.nodes[activePath.nodes.Count - 1].transform.position, transform.position, movementSpeed);            
            FollowPath(activePath, speed, !animator.aiming);            
            if (activePath.nodes.Count == 0)
            {
                waitTime = Random.Range(10, 100);
            }
        }
        else
        {
            waitTime--;
            if (waitTime > 0) return;
            PF_Node node = Pathfind.GetNode(transform.position);
            if (!node)
            {
                if (lastNode)
                    node = lastNode;
                else
                    return;
            }            
            PF_Node randomNode = Pathfind.GetRandomNodeNear(node, Random.Range(5, 20));
            activePath = Pathfind.GetPath(transform.position, randomNode.transform.position);
        }
    }
}
