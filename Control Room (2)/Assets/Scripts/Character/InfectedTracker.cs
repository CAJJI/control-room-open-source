﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfectedTracker : CharacterTracker {

    Infected infected;

    private void Start()
    {
        SetCharacter();
        infected = character.GetComponent<Infected>();
        if (!photonView.isMine)
        {

        }
    }

    private void LateUpdate()
    {
        if (photonView.isMine) return;
        if (infected.activeTarget) return;
        Position();
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (!PhotonNetwork.inRoom) return;
        SyncCharacter(stream, info);

        if (stream.isWriting)
        {
            int targetId = -1;
            if (infected.activeTarget)
             targetId = infected.activeTarget.id;
            stream.SendNext(targetId);
        }
        else
        {
            int targetId = (int)stream.ReceiveNext();
            if (targetId >= 0)
                infected.activeTarget = MultiplayerManager.Instance.GetBehaviour<Character>(targetId);
        }
    }
}
