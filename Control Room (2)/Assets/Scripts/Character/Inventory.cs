﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventorySlot
{
    public Item item;

    public void Drop()
    {
        item = null;
    }

    public InventorySlot(Item item)
    {
        this.item = item;
    }
}

[System.Serializable]
public class Inventory {
    
    public InventoryHUD hud;
    public List<InventorySlot> slots = new List<InventorySlot>();

    public void Drop(Item item)
    {                
        for (int i = 0; i < slots.Count; i++)
        {
            InventorySlot slot = slots[i];
            if (slot.item == item)
            {
                if (hud)
                    hud.SetSlot(i, null);
                slot.Drop();
                //slots.Remove(slot);
            }
        }
    }

    public bool CanDeposit(Item item)
    {
        for(int i = 0; i < slots.Count; i++)
        {
            if (!slots[i].item) return true;
        }
        return false;
    }

    public void Deposit(Item item)
    {
        int index = 0;
        foreach(InventorySlot slot in slots)
        {
            if (slot.item == null)
            {
                if (hud)
                hud.SetSlot(index, item);
                slot.item = item;
                return;
            }
            index++;
        }
        //slots.Add(new InventorySlot(item));
    }

    public bool HasItem(Item item) {
        for (int i = 0; i < slots.Count; i++) {
            if (slots[i].item == item)
                return true;
        }
        return false;
    }

    public void UpdateImage(Item item, Sprite image)
    {
        if (!hud) return;
        hud.UpdateImage(item, image);
    }

    public void SetActiveItem(Item item)
    {
        if (item == null)
        {
            if (hud)
                hud.UnsetActiveSlot();
            return;
        }
        for (int i = 0; i < slots.Count; i++)
        {
            if (hud)
            {
                if (item == slots[i].item)
                    hud.SetActiveSlot(i);
            }
        }
    }
}
