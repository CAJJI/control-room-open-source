﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PasswordMenu : MonoBehaviour {

    public InputField passInputField;
    public Text errorText;
    public UnityEvent continueEvent;
    public RoomInfo roomInfo;

    private void OnEnable()
    {
        errorText.text = "";
        passInputField.text = "";

        passInputField.ActivateInputField();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            Continue();
    }

    public void Continue()
    {
        string pass = passInputField.text;
        string correctPass = MultiplayerManager.GetRoomPassword(roomInfo);

        if (pass != correctPass)
            errorText.text = "Password incorrect.";
        else
        {
            PhotonNetwork.JoinRoom(roomInfo.Name);
            continueEvent.Invoke();
        }

    }
}
