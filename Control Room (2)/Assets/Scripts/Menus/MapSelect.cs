﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public class MapSelectLevel
{
    public Level level;
    public Sprite previewImage;
    public int scene;
}

public class MapSelect : MonoBehaviour {

    public MapSelectLevel[] mapSelectLevels;    
}
