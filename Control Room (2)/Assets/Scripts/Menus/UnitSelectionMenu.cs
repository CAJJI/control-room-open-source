﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectionMenu : MonoBehaviour
{    
    public CRInterface CRinterface;
    public int selectedButton;
    public List<WorldButton> buttons = new List<WorldButton>();

    void Start()
    {
        for (int i = 1; i < transform.childCount; i++)
        {
            WorldButton button = transform.GetChild(i).GetComponent<WorldButton>();
            Color color = MilitaryManager.GetUnit((UnitCode)i - 1).color;
            button.GetComponentInChildren<TextMesh>().color = color;
            button.GetComponent<Renderer>().material.color = color;
            buttons.Add(button);
        }
    }

    void Update()
    {
        Collider playerLooking = Player.Instance.IsLookingAt(10, Player.Instance.interactLayerMask).collider;
        if (!playerLooking) {
            Close();
            return;
        }
        selectedButton = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (playerLooking.gameObject == transform.GetChild(i).gameObject)
            {
                if (i > 0)
                buttons[i-1].highlight.state = true;
                selectedButton = i;
            }
        }
    }

    public void Close()
    {
        CRinterface.mouseHold.Release();
        Destroy(gameObject);
    }
}