﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveCulling : MonoBehaviour {

	void Start () {        
        SkinnedMeshRenderer renderer = GetComponent<SkinnedMeshRenderer>();        
        Mesh mesh = renderer.sharedMesh;
        Bounds newBounds = new Bounds(mesh.bounds.center, Vector3.one * 1000000);
        renderer.localBounds = newBounds;
        mesh.bounds = newBounds;                        

	}
}
