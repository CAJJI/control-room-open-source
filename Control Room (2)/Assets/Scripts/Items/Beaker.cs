﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Beaker : Item {

    public Ingredient ingredient;
    public float temperature;
    public GameObject mixture;
    
    public bool disclude;
    public bool isCure;

    private void Start() {
        if (isCure)
            MultiplayerManager.cure = this;
    }

    public void Complete(Ingredient.Temperature temperature)
    {
        if (ingredient.temperature == temperature) return;

        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCComplete", PhotonTargets.AllBuffered, (int)temperature);
        else
            RPCComplete((int)temperature);
    }

    [PunRPC]
    public void RPCComplete(int temperature)
    {
        Ingredient.Temperature temp = (Ingredient.Temperature)temperature;
        ingredient.temperature = temp;
        if (temp == Ingredient.Temperature.Cold)
            temperature = -100;
        else if (temp == Ingredient.Temperature.Hot)
            temperature = 100;
        //temperature =
    }
}
