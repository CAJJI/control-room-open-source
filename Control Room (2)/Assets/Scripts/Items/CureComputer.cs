﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(CureComputer))]
[CanEditMultipleObjects]
public class CureComputerEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        CureComputer cureComputer = (CureComputer)target;

        if (GUILayout.Button("GenerateIngredients")) {
            cureComputer.GenerateCombinations();

        }
    }
}
#endif



[System.Serializable]
public class IngredientCombinations {
    public List<Ingredient> ingredients = new List<Ingredient>();
    public Ingredient ingredient = new Ingredient();
}

public class CureComputer : MultiplayerBehaviour {

    public Renderer[] lights;
    public Color[] lightColors;
    public GameObject platter;
    public Transform platterPosition;
    public GameObject beakerPlatform;
    public Transform beakerLowerPosition;
    public Transform beakerUpperPosition;
    public GameObject beakerCover;
    public GameObject cureCover;

    public TextMesh textMesh;

    public bool processing;
    public float process;
    public int stage;    

    //public ItemHolder[] itemHolders;
    public ItemHolder itemHolder;
    public ItemHolder cureHolder;
    public IngredientCombinations[] combinations;
    public IngredientCombinations activeCombination;

    public AudioSource audioSource;
    public AudioClip calculatedClip;
    public AudioClip successClip;
    public AudioClip failureClip;
    public AudioClip platformClip;

    public int processCureTime;
    public int mixCureTime;

    private void Start()
    {
        foreach(Renderer rend in lights)
        {
            SetLightColor(rend, lightColors[0]);
        }

        if (PhotonNetwork.isMasterClient || !PhotonNetwork.inRoom)
        {
            SetCure();
        }
    }    

    public void SetCure() {
        int selection = Random.Range(0, combinations.Length);
        int removeAmount = 3 - (GlobalManager.levelInfo.difficulty);
        //if (GlobalManager.levelInfo.difficulty >= 2) removeAmount = 0;
        List<int> remove = new List<int>();
        for (int i = 0; i < removeAmount; i++)
        {
            int searches = 20;
            int index = Random.Range(0, combinations[selection].ingredients.Count);
            while (remove.Contains(index) && searches > 0)
            {                
                index = Random.Range(0, combinations[selection].ingredients.Count);
                searches--;
            }
            Debug.Log(index);
            remove.Add(index);            
        }

        if (PhotonNetwork.inRoom) {
            photonView.RPC("RPCSetCure", PhotonTargets.AllBuffered, selection, remove.ToArray());
        } else {
            RPCSetCure(selection, remove.ToArray());
        }
    }

    List<Ingredient> removeIngredients = new List<Ingredient>();
    [PunRPC]
    public void RPCSetCure(int selection, int[] remove) {
        activeCombination = combinations[selection];

        removeIngredients.Clear();
        for (int i = 0; i < activeCombination.ingredients.Count; i++)
        {
            if (remove.Contains(i))
                removeIngredients.Add(activeCombination.ingredients[i]);
        }
        for(int i = 0; i < removeIngredients.Count; i++)
        {
            activeCombination.ingredients.Remove(removeIngredients[i]);
        }
    }
    
    
    public void GenerateCombinations()
    {
        List<IngredientCombinations> list = new List<IngredientCombinations>();
        for (int i = 0; i < 3; i++)
        {            
            IngredientCombinations newCombination = new IngredientCombinations();
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i)%3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i+1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i+1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i + 1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 2) % 3), color = CureManager.Instance.ingredients[(i+2) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 2) % 3), color = CureManager.Instance.ingredients[(i + 2) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 2) % 3), color = CureManager.Instance.ingredients[(i + 2) % 3].color });

            list.Add(newCombination);
        }

        for (int i = 0; i < 3; i++)
        {
            IngredientCombinations newCombination = new IngredientCombinations();
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i+1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i+1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i+1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i + 1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 2) % 3), color = CureManager.Instance.ingredients[(i + 2) % 3].color });            

            list.Add(newCombination);
        }

        for (int i = 0; i < 3; i++)
        {
            IngredientCombinations newCombination = new IngredientCombinations();
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i) % 3), color = CureManager.Instance.ingredients[(i) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i+1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i+1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i + 1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i + 1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 1) % 3), color = CureManager.Instance.ingredients[(i + 1) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 2) % 3), color = CureManager.Instance.ingredients[(i+2) % 3].color });
            newCombination.ingredients.Add(new Ingredient() { type = (Ingredient.Type)((i + 2) % 3), color = CureManager.Instance.ingredients[(i+2) % 3].color });            

            list.Add(newCombination);
        }

        for (int i = 0; i < list.Count; i++) {
            for (int y = 0; y < 3; y++) {
                int range = Random.Range(0, list[i].ingredients.Count);
                list[i].ingredients.Remove(list[i].ingredients[range]);
            }
        }

            for (int i = 0; i < list.Count; i++)
        {
            Color color = Color.black;
            for (int y = 0; y < list[i].ingredients.Count; y++)
            {
                color += list[i].ingredients[y].color / list[i].ingredients.Count;
                list[i].ingredients[y].temperature = Random.Range(0, 100) < 50 ? Ingredient.Temperature.Cold : Ingredient.Temperature.Hot;
            }
            list[i].ingredient.color = color * 2;
        }                   

        combinations = list.ToArray();
    }

    public void SetLightColor(Renderer rend, Color color)
    {
        rend.material.color = color;
        rend.material.SetColor("_EmissionColor", color);
    }

    private void Update() {

        if (Manager.debugMode)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                GenerateCure();
            if (Input.GetKeyDown(KeyCode.Alpha2))
                SetStage(5);            
        }

        textMesh.text = "";
        if (stage == 0)
        {            
            LogText("Place infected DNA on platter.\n1. Take extractor gun.\n2. Extract DNA from infected corpse.\n3. Inject DNA into grey platter.");
        }
        else if (stage == 1)
        {
            stage = 2;
        }
        else if (stage == 2)
        {
            ProcessCure();
        }
        else if (stage == 3)
        {
            Manager.PlayAudio(audioSource, calculatedClip);
            SetLightColor(lights[0], lightColors[2]);
            process = 0;
            Debug.Log(CombinationString(activeCombination));
            stage = 4;
        }
        else if (stage == 4)
        {
            AwaitIngredients();
        }
        else if (stage == 5)
        {
            SetLightColor(lights[1], lightColors[2]);
            SetLightColor(lights[2], lightColors[1]);
            process = 0;
            stage = 6;
        }
        else if (stage == 6)
        {
            ProcessIngredients();
        }
        else if (stage == 7)
        {            
            Manager.PlayAudio(audioSource, platformClip);
            SetLightColor(lights[2], lightColors[2]);
            process = 0;
            cureCover.GetComponent<Collider>().enabled = false;
            cureHolder.item.GetComponent<Beaker>().mixture.GetComponent<Renderer>().material.color = activeCombination.ingredient.color;
            GlobalManager.cureGenerated = true;
            ObjectiveUI.UpdateText();
            stage = 8;
        }
        else if (stage == 8)
        {            
            cureCover.transform.localRotation = Quaternion.Lerp(cureCover.transform.localRotation, Quaternion.Euler(-90, 0, 0), 0.1f);
            LogText("Cure complete.");
            LogText("Please transport to rescue team.");
        }
    }        

    void ProcessCure() {

        SetLightColor(lights[0], lightColors[1]);
        platter.transform.position = Vector3.Lerp(platter.transform.position, platterPosition.position, 0.1f);
        process += Time.deltaTime;
        LogText("DNA registered.");
        LogText("Processing cure: " + (int)((process / processCureTime) * 100) + "/" + 100);
        if (process > processCureTime) {
            stage++;
            //SetStage(stage + 1);            
        }
    }

    void AwaitIngredients() {
        
        LogText("Cure processed.");
        LogText("Place ingredients in slot.");
        LogText(CombinationString(activeCombination));

        if (processing) return;
        if (itemHolder.item)
            StartCoroutine(ProcessIngredient());
        else {
            beakerCover.transform.localRotation = Quaternion.Lerp(beakerCover.transform.localRotation, Quaternion.Euler(0, 0, 90), 0.1f);
            beakerPlatform.transform.position = Vector3.Lerp(beakerPlatform.transform.position, beakerUpperPosition.position, 0.1f);
        }

        if (activeCombination.ingredients.Count == 0) {
            stage++;
            //SetStage(stage+1);
        }      
    }

    IEnumerator ProcessIngredient() {
        processing = true;
        float time = 0;

        SetLightColor(lights[1], lightColors[1]);
        itemHolder.SetDisable(true);
        while (time < 3) {
            LogText("Processing ingredient");
            beakerCover.transform.rotation = Quaternion.Lerp(beakerCover.transform.rotation, Quaternion.Euler(0, 0, 0), 0.1f);
            beakerPlatform.transform.position = Vector3.Lerp(beakerPlatform.transform.position, beakerLowerPosition.position, 0.1f);
            time += Time.deltaTime;
            yield return null;
        }

        Beaker beaker = itemHolder.item.GetComponent<Beaker>();        

        bool found = false;

        Debug.Log(beaker.ingredient.type);
        Debug.Log(beaker.ingredient.temperature);

        for (int i = 0; i < activeCombination.ingredients.Count; i++) {
            if (found) continue;
            Ingredient ingredient = activeCombination.ingredients[i];
            Debug.Log(ingredient.type);
            Debug.Log(ingredient.temperature);
            Debug.Log(ingredient.type == beaker.ingredient.type);
            Debug.Log(ingredient.temperature == beaker.ingredient.temperature);
            if (ingredient.type == beaker.ingredient.type && ingredient.temperature == beaker.ingredient.temperature) {                
                found = true;
                activeCombination.ingredients.Remove(ingredient);
            }
        }

        if (found)
        {
            itemHolder.RPCDropItem();
            Destroy(beaker.gameObject);
        }

        bool audioPlayed = false;
        time = 0;
        while (time < 2) {
            if (found) {
                LogText("Ingredient added");
                if (!audioPlayed)
                {
                    audioPlayed = true;
                    Manager.PlayAudio(audioSource, successClip);
                }
            } else {
                LogText("Incorrect ingredient");
                if (!audioPlayed)
                {
                    audioPlayed = true;
                    Manager.PlayAudio(audioSource, failureClip);
                }
            }
            beakerCover.transform.rotation = Quaternion.Lerp(beakerCover.transform.rotation, Quaternion.Euler(0, 0, 90), 0.1f);
            beakerPlatform.transform.position = Vector3.Lerp(beakerPlatform.transform.position, beakerUpperPosition.position, 0.1f);
            time += Time.deltaTime;
            yield return null;
        }

        itemHolder.SetDisable(false);
        if (itemHolder.item)
            itemHolder.RPCDropItem();        

        SetLightColor(lights[1], lightColors[0]);

        processing = false;
    }

    void ProcessIngredients() {

        process += Time.deltaTime;
        LogText("Mixing ingredients: " + (int)((process / mixCureTime) * 100) + "/" + 100);

        if (process > mixCureTime) {
            stage++;
            //SetStage(stage + 1);
        }
    }

    public void SetStage(int newStage) {
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCSetStage", PhotonTargets.AllBuffered, newStage);
        else
            RPCSetStage(newStage);
    }

    [PunRPC]
    public void RPCSetStage(int newStage) {
        stage = newStage;
    }

    public void LogText(object text)
    {
        textMesh.text += text + " \n";
    }

    public string CombinationString(IngredientCombinations combination) {
        string result = "";
        //result += "Cure = ";
        for(int i = 0; i < combination.ingredients.Count; i++) {
            result += " >";
            Ingredient ingredient = combination.ingredients[i];
            result += System.Enum.GetName(typeof(Ingredient.Temperature), ingredient.temperature) + " ";
            result += System.Enum.GetName(typeof(Ingredient.Type), ingredient.type) + " Beaker ";
            if (i < combination.ingredients.Count - 1)
                result += "\n";
                //result += "+ ";
        }
        return result;
    }

    public void GenerateCure() {
        if (stage == 0) {
            SetStage(stage + 1);
        }
    }
}
