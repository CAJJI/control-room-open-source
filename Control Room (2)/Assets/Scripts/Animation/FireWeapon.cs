﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWeapon : StateMachineBehaviour {

    public Weapon weapon;
	 	
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {        
        Character character = animator.GetComponentInParent<Character>();
        if (character && character.heldItem)
        {
            if (character.isPlayer) return;
            character.heldItem.Use();
        }
	}
}
