﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimation : StateMachineBehaviour {

    public bool open;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        BlastDoor door = animator.GetComponent<BlastDoor>();
        door.StartCoroutine(door.SetState(open));
    }
}
