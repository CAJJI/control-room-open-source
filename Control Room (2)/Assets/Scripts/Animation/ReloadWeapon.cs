﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadWeapon : StateMachineBehaviour
{

    Character character;
    public Weapon weapon;
    public bool cancelReload;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!character)
            character = animator.GetComponentInParent<Character>();
        if (character)
            character.reloading = true;
        cancelReload = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (character.animator.cancelReload)
            cancelReload = true;
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!character)
            character = animator.GetComponentInParent<Character>();

        if (character)
        {
            character.reloading = false;
            if (cancelReload) return;
            if (character.heldItem)
            {
                Gun gun = character.heldItem.GetComponent<Gun>();
                if (!character.isPlayer)
                    gun.Reload(gun.def.clipSize - gun.ammo);
                else
                {
                    Ammo ammo = character.GetAmmo(gun.def.type);

                    int required = gun.def.clipSize - gun.ammo;

                    if (ammo.amount >= required)
                    {
                        character.GetAmmo(gun.def.type).amount -= required;
                        gun.Reload(required);
                    }
                    else
                    {
                        gun.Reload(ammo.amount);
                        character.GetAmmo(gun.def.type).amount = 0;
                    }
                }

            }
        }
    }
}
