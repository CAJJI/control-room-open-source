﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionManager
{    
    public static List<Detector> soldiers = new List<Detector>();
    static int soldierIndex;
    public static List<Detector> infected = new List<Detector>();
    static int infectedIndex;
    public static List<Character> characters = new List<Character>();
    //public static List<TransmissionScreen> transmissions = new List<TransmissionScreen>();
    public static List<GameObject> detectables = new List<GameObject>();

    static Detector detector;

    public static void Update()
    {
        UpdateSoldiers();        
        UpdateInfected();
    }

    static void UpdateSoldiers()
    {
        //RemoveNull(soldiers);
        int limit = soldierIndex + 4;
        for (int i = soldierIndex; i < limit; i++)
        {
            if (i >= soldiers.Count)
            {
                soldierIndex = 0;
                break;
            }

            detector = soldiers[i];
            if (detector == null)
            {
                soldiers.Remove(detector);
                continue;
            }

            detector.detections.Clear();

            AddDetections(detector, ListGameObjects(infected));
            AddDetections(detector, detectables);
            detector.updated.Set(true);
            soldierIndex++;
        }
    }

    static void UpdateInfected()
    {
        //RemoveNull(infected);
        //RemoveNull(characters);

        int limit = infectedIndex + 10;
        for (int i = infectedIndex; i < limit; i++)
        {
            if (i >= infected.Count)
            {
                infectedIndex = 0;
                break;
            }

            detector = infected[i];
            if (detector == null)
            {
                infected.Remove(detector);
                continue;
            }

            detector.detections.Clear();

            AddDetections(detector, ListGameObjects(soldiers));
            AddDetections(detector, ListGameObjects(characters));
            detector.updated.Set(true);
            infectedIndex++;
        }
    }

    public static void RemoveNull(List<Detector> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == null)
                list.Remove(list[i]);
        }
    }

    public static void RemoveNull(List<Character> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == null)
                list.Remove(list[i]);
        }
    }

    static Detector detectorTarget;
    static List<GameObject> newList = new List<GameObject>();
    public static List<GameObject> ListGameObjects(List<Detector> list)
    {
        newList.Clear();
        for(int i = 0; i < list.Count; i++)
        {
            detectorTarget = list[i];
            if (detectorTarget == null)
            {
                list.Remove(detectorTarget);
                continue;
            }
            newList.Add(detectorTarget.gameObject);
        }
        return newList;
    }

    static Character characterTarget;
    public static List<GameObject> ListGameObjects(List<Character> list)
    {
        newList.Clear();
        for (int i = 0; i < list.Count; i++)
        {
            characterTarget = list[i];
            if (characterTarget == null)
            {
                list.Remove(characterTarget);
                continue;
            }
            newList.Add(characterTarget.gameObject);
        }
        return newList;
    }

    //static TransmissionScreen transmissionTarget;
    //public static List<GameObject> ListGameObjects(List<TransmissionScreen> list)
    //{
    //    newList.Clear();
    //    for (int i = 0; i < list.Count; i++)
    //    {
    //        transmissionTarget= list[i];
    //        if (transmissionTarget == null)
    //        {
    //            list.Remove(transmissionTarget);
    //            continue;
    //        }
    //        newList.Add(transmissionTarget.gameObject);
    //    }
    //    return newList;
    //}

    static Vector3 position;
    public static void AddDetections(Detector detector, List<GameObject> list)
    {        
        for (int i = 0; i < list.Count; i++)
        {
            GameObject target = list[i];
            if (!target)
            {
                list.Remove(target);
                continue;
            }
            position = detector.transform.position + (detector.transform.rotation * detector.offset);
            float distance = (position - target.transform.position).sqrMagnitude;
            if (distance < detector.radius * detector.radius)
            {
                detector.detections.Add(list[i].gameObject);
            }
        }
    }

    public static void AddSoldier(Detector character)
    {
        soldiers.Add(character);
    }

    public static void AddInfected(Detector character)
    {
        infected.Add(character);
    }

    public static void AddCharacter(Character character)
    {
        characters.Add(character);
    }

 public static void AddDetectable(GameObject GO)
    {
        detectables.Add(GO);
    }
}