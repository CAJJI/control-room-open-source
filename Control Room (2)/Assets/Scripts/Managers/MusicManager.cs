﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class UnlockableTrack
{
    public string name;
    public AudioClip track;
    public bool unlocked;
    public LevelCompletion[] conditions;

    public bool Unlock(LevelCompletion level)
    {
        foreach (LevelCompletion condition in conditions)
        {
            string levelName = System.Enum.GetName(typeof(Level), condition.name);
            int difficulty = (int)condition.difficulty + 1;

            if (PlayerPrefs.GetInt(levelName, 0) < difficulty)
                return false;
            if (condition.withCure && !GlobalManager.hasCure)
                return false;
        }
        PlayerPrefs.SetInt(name, 1);
        unlocked = true;
        return true;
    }

    public bool CheckUnlocked()
    {
        if (unlocked) return true;
        if (PlayerPrefs.GetInt(name) > 0)
        {
            unlocked = true;
            return true;
        }
        return false;

    }
}

public class MusicManager : Singleton<MusicManager> {    

    public AudioSource audioSource;
    public UnlockableTrack[] tracks;
    public int currentTrack;

    public bool pause;
    public Image pauseButton;
    public Sprite pauseSprite;
    public Sprite playSprite;

    public Text trackText;

    private void Awake()
    {
        foreach(UnlockableTrack track in tracks)
        {
            track.CheckUnlocked();
        }

        int randomTrack = Random.Range(0, tracks.Length);
        int searchTrack = 0;
        int unlockedTracks = 0;
        UnlockableTrack chosenTrack = tracks[0];
        while (unlockedTracks < randomTrack)
        {
            chosenTrack = tracks[searchTrack];
            currentTrack = searchTrack;
            if (tracks[searchTrack].unlocked)
            {
                unlockedTracks++;
            }
            searchTrack++;
            if (searchTrack >= tracks.Length) searchTrack = 0;
        }
        PlayTrack(chosenTrack);

        if (PlayerPrefs.GetInt("pauseMusic") == 1)
            Pause();
    }

    private void Update()
    {        
        if (!audioSource.isPlaying && !pause)
        {
            PlayNextTrack(1);
        }
    }

    public void Pause()
    {
        pause = !pause;        
        if (pause)
        {
            PlayerPrefs.SetInt("pauseMusic", 1);
            pauseButton.sprite = playSprite;
            audioSource.Pause();
        }
        else
        {
            PlayerPrefs.SetInt("pauseMusic", 0);
            pauseButton.sprite = pauseSprite;
            audioSource.UnPause();
        }
        
    }

    public void PlayNextTrack(int direction)
    {
        bool unlocked = false;
        while (!unlocked)
        {
            currentTrack+= direction;
            if (currentTrack > tracks.Length - 1) currentTrack = 0;
            if (currentTrack < 0) currentTrack = tracks.Length - 1;
            if (tracks[currentTrack].unlocked)
            {
                unlocked = true;
                PlayTrack(tracks[currentTrack]);
            }
        }
    }

    public void PlayTrack(UnlockableTrack track)
    {
        trackText.text = track.name;

        if (pause)
            Pause();
        audioSource.Stop();
        audioSource.clip = track.track;        
        audioSource.Play();
    }
}
