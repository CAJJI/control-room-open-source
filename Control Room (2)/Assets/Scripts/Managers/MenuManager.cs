﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlickerImage
{
    public GameObject gameObject;
    public Vector3 initialPosition;
    public float amount;

    public FlickerImage(GameObject gameObject, Vector3 initialPosition)
    {
        this.gameObject = gameObject;
        this.initialPosition = initialPosition;
        amount = 2;
    }

    public FlickerImage(GameObject gameObject, Vector3 initialPosition, float amount)
    {
        this.gameObject = gameObject;
        this.initialPosition = initialPosition;
        this.amount = amount;
    }
}

public class MenuManager : Singleton<MenuManager> {

    public GameObject mainMenu;
    public GameObject roomMenu;
    public GameObject lostConnectionWindow;
    public GameObject activeMenu;
    public GameObject lobbyMenu;
    public AudioSource audioSource;
    public AudioClip hoverClip;
    public AudioClip clickClip;

    List<GameObject> flicker;
    List<FlickerImage> flickerImages = new List<FlickerImage>();
    public float maxFlicker;

    public CameraInterface[] cameraInterfacesArray;
    public List<CameraInterface> cameraInterfaces = new List<CameraInterface>();
    public int currentCamera;

    public LevelStats levelStats;
    public LevelStats multiLevelStats;

    public Text currentVersion;
    public Text patchNotesText;
    public Text updateAvailableText;
    public Text announcementText;

    private void Start()
    {
        currentVersion.text = "v"+GlobalManager.Instance.gameVersion.ToString();

        StartCoroutine("UpdateText");

        UnityEngine.Cursor.lockState = CursorLockMode.None;
        UnityEngine.Cursor.visible = true;
        cameraInterfacesArray = GameObject.FindObjectsOfType<CameraInterface>();        
        foreach(CameraInterface cam in cameraInterfacesArray)
        {
            if (!cam.GetComponentInParent<Soldier>())
            {
                cameraInterfaces.Add(cam);
            }
        }

        currentCamera = Random.Range(0, cameraInterfaces.Count);
        StartCoroutine(SwitchCameras());

        CheckLevelComplete();
    }

    IEnumerator UpdateText()
    {
        
        Debug.Log("Check for updates");
        WWW gameVersion = new WWW("http://www.jessetoyota.com/controlroom/gameversion.txt");
        yield return gameVersion;        
        if (gameVersion.error == null)
        {
            Debug.Log("Connected");
            string gameVersionString = gameVersion.text;

            WWW announcement = new WWW("http://www.jessetoyota.com/controlroom/announcement.txt");
            yield return announcement;
            string announcementString = announcement.text;

            WWW patchNotes = new WWW("http://www.jessetoyota.com/controlroom/patchnotes.txt");
            yield return patchNotes;
            string patchNotesString = patchNotes.text;

            if (GlobalManager.Instance.gameVersion.ToString() != gameVersionString)
                updateAvailableText.text = "Update available!";

            announcementText.text = announcementString;
            patchNotesText.text = "Download updates at: cajji.itch.io/control-room \n" + patchNotesString;            

        }    
        else
            Debug.Log("Could not connect " + gameVersion.error);
            
        yield return null;
    }    
    
    void CheckLevelComplete()
    {
        Debug.Log(GlobalManager.levelCompleted);
        if (GlobalManager.levelCompleted)
        {
            if (PhotonNetwork.inRoom)
            {
                OpenMenu(multiLevelStats.gameObject);
                multiLevelStats.LoadStats();
                GlobalManager.currentRoom = PhotonNetwork.room;                                
            }
            else
            {
                OpenMenu(levelStats.gameObject);
                levelStats.LoadStats();
            }
            GlobalManager.levelCompleted = false;
            GlobalManager.survived = false;
            GlobalManager.hasCure = false;
            GlobalManager.cureGenerated = false;
        }
    }

    public void AddFlicker(GameObject GO, float amount)
    {
        flickerImages.Add(new FlickerImage(GO, GO.transform.position, amount));
    }

    IEnumerator SwitchCameras()
    {
        bool stopSwitching = false;
        while (!stopSwitching)
        {
            Transform trans = cameraInterfaces[currentCamera].camPosition;
            Camera.main.transform.position = trans.position;
            Camera.main.transform.rotation = trans.rotation;
            Camera.main.transform.SetParent(trans);
            currentCamera++;
            if (currentCamera > cameraInterfaces.Count - 1) currentCamera = 0;
            yield return new WaitForSeconds(8);
        }
    }

    void Update()
    {       
        FlickerImage();    
        
        if (GlobalManager.connected)
        {
            if (!PhotonNetwork.connected || !PhotonNetwork.insideLobby)
            {
                GlobalManager.connected = false;
                OpenMenu(lostConnectionWindow);
            }
        }
    }

    public void DisconnectFromLobby()
    {
        PhotonNetwork.Disconnect();
        GlobalManager.connected = false;
        OpenMenu(lostConnectionWindow);
    }

    void FlickerImage()
    {
        foreach(FlickerImage image in flickerImages)
        {
            image.gameObject.transform.position = image.initialPosition + (Vector3.right * Random.Range(0, image.amount));
        }
    }

    public void TriggerHover()
    {
        audioSource.clip = hoverClip;        
        audioSource.Play();
    }
    public void TriggerClick()
    {
        audioSource.clip = clickClip;
        audioSource.Play();
    }

    public void LoadScene(int scene)
    {
        GlobalManager.Instance.LoadScene(scene);
    }

    public void SetLevel(int scene)
    {
        GlobalManager.levelInfo.scene = scene;        
    }

    public void SetDifficulty(int difficulty)
    {
        GlobalManager.levelInfo.difficulty = difficulty;
    }

    public void StartGame()
    {
        LoadScene(GlobalManager.levelInfo.scene);
    }    

    public void OpenMenu(GameObject menu)
    {
        if (activeMenu)
            activeMenu.SetActive(false);
        activeMenu = menu;
        activeMenu.SetActive(true);
    }

    public void StartTutorial()
    {
        SetDifficulty(3);
        GlobalManager.levelInfo.scene = 2;

        for (int i = 0; i < 4; i++)
        {
            GlobalManager.levelInfo.soldierInfos[i] = new SoldierInfo();
            GlobalManager.levelInfo.soldierInfos[i].soldierClasses = new List<SoldierInfo.SoldierClass>();
            SoldierInfo.SoldierClass soldierClass = SoldierInfo.SoldierClass.Assault;
            if (i == 3) soldierClass = SoldierInfo.SoldierClass.Engineer;
            GlobalManager.levelInfo.soldierInfos[i].soldierClasses.Add(soldierClass);
        }

        StartGame();
    }

    public void ReturnToRoom()
    {                
        if (PhotonNetwork.inRoom || PhotonNetwork.JoinRoom(MultiplayerManager.activeRoom.Name))
        {            
            OpenMenu(roomMenu);            
        }
        else
        {                        
            LeaveRoom();
        }
    }

    public void LeaveRoom()
    {
        if (PhotonNetwork.inRoom)
        PhotonNetwork.LeaveRoom();
        OpenMenu(lobbyMenu);
        //LobbyMenu.Instance.GenerateList();
    }

    public void Quit()
    {        
        Application.Quit();
    }
}
