﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnPoint {

    public Transform spawnPoint;
    public Vector3 spawnOffset;
    public List<PFPath> destinationPaths = new List<PFPath>();
    public List<PF_Node> ignoreNodes = new List<PF_Node>();


    public void AddSpawnPaths(Transform destination, int spawnPointCount)
    {
        for (int i = 0; i < spawnPointCount; i++)
        {
            Vector3 start = spawnPoint.position + RandomVector3(spawnOffset);
            PF_Node startNode = Pathfind.GetNearestAvailable(Pathfind.GetNode(start), start, ignoreNodes);

            Debug.Log(start);
            Debug.Log(startNode);

            Debug.Log(i);
            PFPath path = Pathfind.GetPath(startNode.transform.position, destination.position);            
            ignoreNodes.Add(path.nodes[0]);


            while (destination.childCount > 0) {             
                startNode = Pathfind.GetNearestAvailable(Pathfind.GetNode(destination.position), destination.position, ignoreNodes);
                destination = destination.GetChild(0);
                path.nodes.AddRange(Pathfind.GetPath(startNode.transform.position, destination.position).nodes);                
            }

            destinationPaths.Add(path);
        }
    }
    List<Infected> spawned = new List<Infected>();
    List<int> ignoreSpawn = new List<int>();
    public List<Infected> Spawn(GameObject prefab, int amount, float multiplier = 1)
    {
        spawned.Clear();
        ignoreSpawn.Clear();        

        amount = Mathf.CeilToInt(amount * multiplier);        

        for (int i = 0; i < amount; i++)
        {
            int searches = destinationPaths.Count;
            int randomValue = Random.Range(0, destinationPaths.Count);
            while (searches > 0 && ignoreSpawn.Contains(randomValue))
            {
                searches--;
                randomValue = Random.Range(0, destinationPaths.Count);                
            }            
            ignoreSpawn.Add(randomValue);
            PFPath setPath = destinationPaths[randomValue];
            Character character = null;
            if (PhotonNetwork.inRoom)
                character = PhotonNetwork.InstantiateSceneObject(prefab.name, setPath.nodes[0].transform.position, Quaternion.identity, 0, null).GetComponent<Character>();
            else
                character = MonoBehaviour.Instantiate(prefab, setPath.nodes[0].transform.position, Quaternion.identity).GetComponent<Character>();
            
            character.mainPath = PFPath.Copy(setPath);
            character.activePath = character.mainPath;
            spawned.Add(character.GetComponent<Infected>());
        }
        return spawned;
    }    

    public Vector3 RandomVector3(Vector3 vector)
    {
        return new Vector3(Random.Range(-vector.x, vector.x), Random.Range(-vector.y, vector.y), Random.Range(-vector.z, vector.z));
    }
}

[System.Serializable]
public class SpawnGroup
{
    public int amount;
    public int delayTime;
}

public class InfectedManager : MonoBehaviour {

    public GameObject infectedPrefab;    
    public bool bakePathsOnStart;
    public float spawnTimer;
    public List<Transform> destinationTransforms;
    public List<SpawnPoint> spawnPoints = new List<SpawnPoint>();

    public SpawnGroup[] spawnGroups;
    public int currentSpawnGroup;

    public List<Infected> infectedInScene;
    public int maxInfectedInScene = 30;
    public bool limitInfected;

    private void Start()
    {
        if (bakePathsOnStart)
        BakePaths();
        spawnTimer = spawnGroups[0].delayTime;    
        
        if (PhotonNetwork.inRoom)
        {
            maxInfectedInScene += 10 * PhotonNetwork.room.PlayerCount;
        }
    }

    public void BakePaths()
    {
        foreach (SpawnPoint spawnPoint in spawnPoints)
        {
            spawnPoint.ignoreNodes = new List<PF_Node>();
            spawnPoint.destinationPaths = new List<PFPath>();
            foreach (Transform destination in destinationTransforms)
            {
                spawnPoint.AddSpawnPaths(destination, 6);
            }
        }
    }

    private void Update()
    {
        SpawnRate();
    }

    void SpawnRate()
    {        
        if (PhotonNetwork.inRoom && !PhotonNetwork.isMasterClient) return;

        if (!Manager.lockDown) return;

        spawnTimer += Time.deltaTime;

        if (currentSpawnGroup >= spawnGroups.Length - 1) currentSpawnGroup = 0;
        if (spawnTimer > spawnGroups[currentSpawnGroup].delayTime)
        {
            for (int i = 0; i < infectedInScene.Count; i++)
            {
                if (infectedInScene[i] == null)
                    infectedInScene.Remove(infectedInScene[i]);
            }

            if (infectedInScene.Count > maxInfectedInScene && limitInfected) return;

            float multiplier = 1;
            if (PhotonNetwork.inRoom)
            {
                int expectedPlayerCount = 2;
                multiplier = ((float)PhotonNetwork.room.PlayerCount / expectedPlayerCount);                
            }
            multiplier += GlobalManager.levelInfo.difficulty / 4;            

            List<Infected> list = spawnPoints[Random.Range(0, spawnPoints.Count)].Spawn(infectedPrefab, spawnGroups[currentSpawnGroup].amount, multiplier);            
            infectedInScene.AddRange(list);
            currentSpawnGroup++;
            spawnTimer = 0;            
        }
        
    }

}
