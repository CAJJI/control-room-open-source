﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {
    
    char[] textChars;
    public int textIndex;
    public Text tutText;    

    public void SetText(string text)
    {
        tutText.text = "";
        textIndex = 0;
        textChars = text.ToCharArray();
        tutText.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (textChars == null || textIndex > textChars.Length - 1) return;
        tutText.text += textChars[textIndex];
        textIndex++;
    }
}
