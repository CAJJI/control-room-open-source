﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(QuickFix))]
[CanEditMultipleObjects]
public class QuickFixEditor : Editor
{
    public override void OnInspectorGUI()
    {
        QuickFix target = (QuickFix)this.target;
        if (GUILayout.Button("Fix Lights"))
            if (EditorUtility.DisplayDialog("Confirm?", "Confirm?", "Yes", "No"))
                target.FixLights();

        if (GUILayout.Button("Replace Lights"))
            if (EditorUtility.DisplayDialog("Confirm?", "Confirm?", "Yes", "No"))
                target.ReplaceLights();

        if (GUILayout.Button("Adjust Lights"))
            if (EditorUtility.DisplayDialog("Confirm?", "Confirm?", "Yes", "No"))
                target.AdjustLights();

        if (GUILayout.Button("Switch Layers"))
            if (EditorUtility.DisplayDialog("Confirm?", "Confirm?", "Yes", "No"))
                target.SwitchLayers();

        if (GUILayout.Button("Fix Multiplayer Behaviours"))
            if (EditorUtility.DisplayDialog("Confirm?", "Confirm?", "Yes", "No"))
                target.FixMultiplayerBehaviours();


        //target.lightPrefab = (GameObject)EditorGUILayout.ObjectField("lightPrefab", target.lightPrefab, typeof(GameObject));
        //target.lightPrefab = EditorGUILayout.PropertyField(serializedObject.FindProperty("lightPrefab"));
        //target.lightPrefab = (GameObject)EditorGUILayout.ObjectField("lightPrefab", target.lightPrefab, typeof(GameObject));
        DrawDefaultInspector();
    }
}
#endif

public class QuickFix : MonoBehaviour
{

    public GameObject lightPrefab;

    public void FixLights()
    {
        List<Power> powers = new List<Power>();
        foreach (Power power in GetComponentsInChildren<Power>())
        {
            if (power.transform.parent != transform) continue;
            Power newPower = power.transform.GetChild(0).gameObject.AddComponent<Power>();
            newPower.onEnable = power.onEnable;
            powers.Add(power);
        }

        for (int i = 0; i < powers.Count; i++)
        {
            DestroyImmediate(powers[i]);
        }
    }

    public void ReplaceLights()
    {
        List<Transform> children = new List<Transform>();
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
        {
            Transform child = transform.GetChild(i);
            children.Add(child);
            Light light = Instantiate(lightPrefab, child.position, child.rotation, transform).GetComponentInChildren<Light>();
        }

        for (int i = 0;i < children.Count; i++)
        {
            DestroyImmediate(children[i].gameObject);
        }
    }

    public void AdjustLights()
    {
        foreach(Light light in GetComponentsInChildren<Light>())
        {
            light.color = Color.red;
            light.gameObject.SetActive(false);
            light.GetComponentInParent<Power>().backUp = true;
        }
    }

    public string layerName;

    public void SwitchLayers()
    {
        foreach (Light light in GetComponentsInChildren<Light>())
        {
            light.gameObject.layer = LayerMask.NameToLayer(layerName);
        }
    }

    public void FixMultiplayerBehaviours()
    {
        foreach(MultiplayerBehaviour behaviour in FindObjectsOfType<MultiplayerBehaviour>())
        {
            if (!behaviour.GetComponent<PhotonView>())
            behaviour.gameObject.AddComponent<PhotonView>();
        }
    }
}