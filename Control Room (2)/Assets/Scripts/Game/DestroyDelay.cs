﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDelay : MonoBehaviour {

    public ParticleType type;

    public float delayTime;
    float currentTime;

    public float scaleMultiplier;
    new public ParticleSystem particleSystem;

    public ParticleEmitter emitter;

    public void Play()
    {
        gameObject.SetActive(true);
        if (!particleSystem)
        particleSystem = GetComponent<ParticleSystem>();
        if (particleSystem)
        {
            particleSystem.Stop();
            particleSystem.Clear();            
            particleSystem.Play();            
        }
        currentTime = 0;
        if (emitter)
            emitter.playing = true;
    }

    private void Update()
    {
        //transform.localScale *= scaleMultiplier;
        if (currentTime >= delayTime)
        {
            //Destroy(gameObject);            
            gameObject.SetActive(false);
            if (emitter) 
            emitter.playing = false;
        }
        currentTime++;
        //currentTime += 60 * Time.deltaTime;
    }
}
