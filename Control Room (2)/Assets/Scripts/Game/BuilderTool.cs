﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnHistory
{
    public GameObject prefab;
    public GameObject gameObject;
    public Vector3 position;
    public Quaternion rotation;

    public SpawnHistory(GameObject prefab, GameObject gameObject, Vector3 position, Quaternion rotation)
    {
        this.prefab = prefab;
        this.gameObject = gameObject;
        this.position = position;
        this.rotation = rotation;
    }
}

public class InputHold
{
    public bool state;
    float pressedTime;
    public float timePassed { get { return Time.timeSinceLevelLoad - pressedTime; } }
    public void Press()
    {
        state = true;
        pressedTime = Time.timeSinceLevelLoad;
    }

    public void Release()
    {
        state = false;
    }

    public static implicit operator bool (InputHold inputHold)
    {
        return inputHold.state;
    }
}

[System.Serializable]
public class BT_List
{    
    public List<GameObject> list;
    public int index;
    public GameObject activeItem { get{ return list[index]; } }
}

public class BuilderTool : MonoBehaviour {

    public Text hudText;
    public LayerMask layerMask;
    public bool active;
    public bool spawnMode;
    public bool deleteMode;
    public bool gridLock;
    public bool lineLock;    
    public float gridSpacing = 0.5f;
    public int index;
    Vector3 activeEulerAngles;
    Quaternion activeRotation { get { return Quaternion.Euler(activeEulerAngles); } }
    InputHold rotating = new InputHold();
    GameObject activeItem { get { return propList[index].activeItem; } }
    GameObject heldItem;
    GameObject lastSpawned;    
    List<GameObject> previews = new List<GameObject>();    
    public BT_List activeList { get { return propList[index]; } }
    public List<BT_List> propList;

    public List<SpawnHistory> history = new List<SpawnHistory>();
    public int historyIndex;

    Transform _rooms;
    public Transform rooms { get { if (!_rooms) _rooms = GetTransform("Rooms"); return _rooms; }}
    Transform _props;
    public Transform props { get { if (!_props) _props = GetTransform("Props"); return _props; } }
    Transform _interfaces;
    public Transform interfaces { get { if (!_interfaces) _interfaces = GetTransform("Interfaces"); return _interfaces; } }
    Transform _items;
    public Transform items { get { if (!_items) _items = GetTransform("Items"); return _items; } }

    private void Start()
    {
        this.enabled = Application.isEditor;        
    }

    private void Update()
    {
        Activate();
        DebugText();
        if (!active) return;
        
        ClearPreviews();
        BuildMode();
        ActiveItem();
        ItemSelect();
        ActiveRotation();
        History();
    }

    void DebugText()
    {
        string text = "";
        if (active)
        {
            text += AddLine("GhostMode: " + BoolToColoredString((Player.Instance.gameObject.layer == LayerMask.NameToLayer("Ghost"))));
            text += AddLine("SpawnMode: " + BoolToColoredString(spawnMode));
            text += AddLine("DeleteMode: " + BoolToColoredString(deleteMode));
            text += AddLine("GridLock: " + BoolToColoredString(gridLock));            
            text += AddLine("GridSpacing: " + gridSpacing.ToString());
            text += AddLine("ActiveList: " + index.ToString());
            text += AddLine("ActiveItem: " + activeList.index.ToString() + " " + activeItem.name);
        }

        hudText.text = text;
    }

    string BoolToColoredString(bool state)
    {
        string color = "<color=red>";
        if (state) color = "<color=green>";
        return color + state.ToString() + "</color>";
    }

    void Activate()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            active = !active;            
            ClearPreviews();            
        }
    }

    void ActiveRotation()
    {
        if (Input.GetMouseButtonDown(1)) rotating.Press();
        if (Input.GetMouseButtonUp(1) && rotating)
        {
            float target = 45;
            if (Input.GetKey(KeyCode.LeftShift)) target *= 2;
            if (rotating.timePassed < 0.2f)
            {
                if (Input.GetKey(KeyCode.LeftAlt))
                    activeEulerAngles = new Vector3(RoundValue(activeEulerAngles.x + target, target),activeEulerAngles.y, activeEulerAngles.z);
                else if (Input.GetKey(KeyCode.LeftControl))
                    activeEulerAngles = new Vector3(activeEulerAngles.x,activeEulerAngles.y , RoundValue(activeEulerAngles.z + target, target));
                else
                    activeEulerAngles = new Vector3(0, RoundValue(activeEulerAngles.y + target, target), 0);
            }
            rotating.Release();
        }

        if (rotating)
        {
            Player.Instance.freezeCamera.state = true;
            if (Input.GetKey(KeyCode.LeftShift))
                activeEulerAngles += new Vector3(Input.GetAxisRaw("Mouse Y"), 0, Input.GetAxisRaw("Mouse X"));            
            else
                activeEulerAngles += new Vector3(Input.GetAxisRaw("Mouse Y"), Input.GetAxisRaw("Mouse X"), 0);
            if (Input.GetKey(KeyCode.LeftShift))
                activeEulerAngles.x = 0;
        }
    }
    
    void ItemSelect()
    {        
        for(int i = 0; i < propList.Count; i++)
        {
            if (Input.GetKeyDown((i + 1).ToString()))
            {
                index = RoundIndex(i,  propList.Count);
            }
        }

        int multiplier = 10;
        if (Input.GetKey(KeyCode.LeftShift)) multiplier *= 5;
        activeList.index = RoundIndex(activeList.index - ((int)(Input.GetAxisRaw("Mouse ScrollWheel") * multiplier)), activeList.list.Count);        
    }
   

    void BuildMode()
    {
        GameObject player = Player.Instance.gameObject;

        lineLock = false;
        if (Input.GetKeyDown(KeyCode.G)) Ghost(player);

        if (Input.GetKeyDown(KeyCode.C)) { spawnMode = !spawnMode; if (spawnMode) deleteMode = false; }

        if (Input.GetKeyDown(KeyCode.Z)) { deleteMode = !deleteMode; if (deleteMode) spawnMode = false; }

        if (Input.GetKeyDown(KeyCode.X)) gridLock = !gridLock;

        if (Input.GetKey(KeyCode.LeftShift) && !rotating) lineLock = true;

        int multiplier = 1;
        if (Input.GetKey(KeyCode.LeftShift)) multiplier = 2;
        if (Input.GetKeyDown(KeyCode.Equals) || Input.GetKeyDown(KeyCode.KeypadPlus)) gridSpacing += 0.5f * multiplier;
        if (Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus)) gridSpacing -= 0.5f * multiplier;
        if (gridSpacing < 0.5f) gridSpacing = 0.5f;
    }
    
    public void Ghost(GameObject player)
    {
        if (player.layer == LayerMask.NameToLayer("Character"))
        {
            Player.Instance.ghost = true;
            Player.Instance.rigidbody.isKinematic = true;
            player.layer = LayerMask.NameToLayer("Ghost");
            Manager.playerSpeedBuff = 2;
        }
        else
        {
            Player.Instance.ghost = false;
            Player.Instance.rigidbody.isKinematic = false;
            player.layer = LayerMask.NameToLayer("Character");
            Manager.playerSpeedBuff = 0;
        }
    }

    void ActiveItem()
    {
        RaycastHit hit = Player.Instance.IsLookingAt(100, layerMask);
        if (!hit.collider) return;

        if (spawnMode)
        {            
            AddToPreviews(activeItem, hit.point, activeRotation);
        }        

        if (Input.GetMouseButtonDown(0))
        {
            if (deleteMode)
            {
                GameObject parent = GetParent(hit.collider.gameObject, new string[]{ rooms.name, props.name, interfaces.name, items.name, "Environment", "Ground"});                
                if (parent)
                Destroy(parent);
                deleteMode = false;
                spawnMode = true;
                return;
            }

            if (spawnMode)
            {
                Debug.Log(hit.collider.name);
                if (lineLock)
                {                    
                    Vector3 position = lastSpawned.transform.position;
                    Vector3 direction = hit.point - position;
                    float distance = RoundValue(direction.magnitude, gridSpacing) / gridSpacing;                                        
                    for (int i = 1; i <= distance; i++)
                    {
                        position += direction.normalized * gridSpacing;                        
                        SpawnItem(activeItem, position, lastSpawned.transform.rotation, true);
                    }                    

                }
                else
                SpawnItem(activeItem, hit.point, activeRotation, true);
            }
            else
            {
                Rigidbody rb = hit.collider.GetComponent<Rigidbody>();
                if (rb)
                {
                    heldItem = rb.gameObject;
                }
            }
        }
    }

    public GameObject SpawnItem(GameObject gameObject, Vector3 position, Quaternion rotation, bool saveAsLast)
    {
        position = OffsetPosition(position);                
        
        GameObject newGO = Instantiate(gameObject, position, rotation) as GameObject;
        if (saveAsLast)
        {
            if (index == 0) newGO.transform.parent = rooms;
            if (index == 1) newGO.transform.parent = props;
            if (index == 2) newGO.transform.parent = interfaces;
            if (index == 3) newGO.transform.parent = items;
            lastSpawned = newGO;
            ClearHistory(historyIndex);
            history.Add(new SpawnHistory(gameObject, newGO, position, rotation));
            historyIndex++;
            Debug.Log("Spawn " + newGO.name);
        }
        return newGO;
    }

    #region Preview
    void ClearPreviews()
    {
        for (int i = 0; i < previews.Count; i++)
        {
            Destroy(previews[i]);
        }
        previews = new List<GameObject>();
    }

    void AddToPreviews(GameObject GO, Vector3 position, Quaternion rotation)
    {
        GameObject preview = SpawnItem(GO, position, rotation, false);
        foreach (Collider col in preview.GetComponentsInChildren<Collider>())
            col.enabled = false;
        previews.Add(preview);
    }
    #endregion

    #region History
    void History()
    {
        if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            if (historyIndex <= 0) return;
            historyIndex--;
            Debug.Log("Remove " + history[historyIndex].gameObject.name);
            Destroy(history[historyIndex].gameObject);
        }
        if (Input.GetKeyDown(KeyCode.RightBracket))
        {
            if (historyIndex >= history.Count) return;
            SpawnHistory spawn = history[historyIndex];            
            spawn.gameObject = Instantiate(spawn.prefab, spawn.position, spawn.rotation) as GameObject;
            Debug.Log("Spawn " + spawn.gameObject.name);
            historyIndex++;

        }
    }

    public void ClearHistory(int startIndex)
    {
        List<SpawnHistory> newHistory = new List<SpawnHistory>();
        for (int i = 0; i < startIndex; i++)
        {
            newHistory.Add(history[i]);
        }
        history = newHistory;
    }
    #endregion

    #region Misc
    public Transform GetTransform(string transform)
    {
        GameObject find = GameObject.Find(transform);
        if (find)
            return find.transform;
        else
            return new GameObject(transform).transform;
    }
    public string AddLine(string text)
    {
        return text + "\n";
    }

    public GameObject GetParent(GameObject GO)
    {
        return GetParent(GO, "");
    }

    public GameObject GetParent(GameObject GO, string ignoreTransform)
    {
        return GetParent(GO, new string[] { ignoreTransform });
    }

    public GameObject GetParent(GameObject GO, string[] ignoreTransform)
    {
        if (StringArrayContains(ignoreTransform, GO.name)) return null;
        Transform parent = GO.transform;
        bool ignore = false;
        int count = 0;
        while (parent.parent != null && !ignore && count < 10)
        {
            count++;
            ignore = StringArrayContains(ignoreTransform, parent.parent.name);
            if (!ignore)
                parent = parent.parent;
        }
        return parent.gameObject;
    }

    Vector3 OffsetPosition(Vector3 position)
    {
        if (!gridLock)
            return position;
        position.x = RoundValue(position.x, gridSpacing);
        position.y = RoundValue(position.y, gridSpacing);
        position.z = RoundValue(position.z, gridSpacing);
        return position;
    }

    public int RoundIndex(int index, int maxValue)
    {
        if (index >= maxValue)
            index = 0;
        if (index < 0)
            index = maxValue - 1;
        return index;
    }

    Vector3 RoundDirection(Vector3 direction)
    {
        direction.y = 0;
        float angle = Vector3.Angle(Vector3.forward, direction);
        angle = RoundValue(angle, 90);
        return Quaternion.Euler(0, angle, 0) * Vector3.forward;
    }

    float RoundValue(float value, float round)
    {
        return Mathf.Round(value / round) * round;
    }

    bool StringArrayContains(string[] array, string check)
    {
        foreach (string index in array)
        {
            if (index.Contains(check))
                return true;
        }
        return false;
    }
#endregion
}
