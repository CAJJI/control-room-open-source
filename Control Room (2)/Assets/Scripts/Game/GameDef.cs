﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.PostProcessing;
#if UNITY_EDITOR
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(GameDef))]
public class GameDefEditor: Editor
{
    public override void OnInspectorGUI()
    {
        GameDef target = (GameDef)this.target;

        if (GUILayout.Button("Update Custom Properties"))
        {
            target.UpdateCustomProperties();
        }

        DrawDefaultInspector();
    }
}
#endif

public enum ParticleType
{
    Metal,
    Flesh,
    DoorBreak,
}

[System.Serializable]
public class ParticleDef
{
    public ParticleType type;
    public GameObject prefab;
}

[System.Serializable]
public class Sounds
{
    public AudioClip pickUpAmmo;
}

public enum CustomPropertyType
{
    PasswordProtected,
    Password,
    Map,
    Difficulty,
    Starting,
    InGame,
    LENGTH
}

[System.Serializable]
public class CustomProperty
{
    public CustomPropertyType type;
    public string id;
}


[CreateAssetMenu(fileName = "GameDef", menuName = "Control Room/GameDef")]
public class GameDef : ScriptableObject {

    public GameObject cameraPrefab;

    public GameObject muzzleFlashPrefab;
    public GameObject particlePrefab;
    public LayerMask damageLayerMask;

    public AudioMixer audioMixer;
    AudioMixerGroup mixerGroup;
    public AudioClip testClip;
    public AudioClip hitClip;

    public PostProcessingProfile postProcessingProfile;

    public Sounds sounds;

    public CharacterDef[] characters;

    public LevelInfo[] levels;

    public GunDef[] gunDefs;    
    
    public PlayerLabel playerLabelPrefab;

    public string[] filteredWords;

    public List<CustomProperty> customProperties = new List<CustomProperty>();


    public AudioMixerGroup GetMixer()
    {
        if (!mixerGroup)
            mixerGroup = audioMixer.FindMatchingGroups("Master")[0];
        return mixerGroup;
    }    

    public LevelInfo GetLevel(Level level)
    {
        foreach(LevelInfo info in levels)
        {
            if (info.level == level)
                return info;
        }
        return null;
    }

    public GunDef GetGunDef(GunType type)
    {
        GunDef[] gunDefs = this.gunDefs;        
        foreach(GunDef def in gunDefs)
        {
            if (def.type == type)
                return def;
        }
        return null;
    }
    
    public string GetCustomProperty(CustomPropertyType type)
    {
        foreach (CustomProperty property in customProperties)
        {
            if (property.type == type)
                return property.id;
        }
        return null;
    }

    public void UpdateCustomProperties()
    {
        for (int i = 0; i <(int)CustomPropertyType.LENGTH; i++)
        {
            if (GetCustomProperty((CustomPropertyType)i) == null)
                customProperties.Add(new CustomProperty() { type = (CustomPropertyType)i });
        }
    }

    public CharacterDef GetCharacterDef(CharacterType type)
    {
        foreach(CharacterDef def in characters)
        {
            if (def.type == type)
                return def;
        }
        return null;
    }
}
