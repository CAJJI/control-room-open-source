﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTrigger : MonoBehaviour {

    public bool triggered;
    public float delay;
    float delayTime;
    public UnityEvent triggerEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (triggered) return;
        if (other.GetComponent<Player>())
        {
            triggered = true;
        }
    }

    private void Update()
    {
        if (triggered)
            delayTime+= Time.deltaTime;
        if (delayTime > delay)
            Trigger();
    }

    public void TriggerDelay()
    {
        triggered = true;
    }

    public void Trigger()
    {
        triggerEvent.Invoke();
        Destroy(gameObject);
    }
}
