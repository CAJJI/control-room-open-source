﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WorldButton : Interactable {

    public UnityEvent Event;
    [HideInInspector]
    public Color initialColor;
    public ResetBool highlight = new ResetBool();
    bool resetColor;
    [HideInInspector]
    new public Renderer renderer;

    private void Start()
    {        
        renderer = GetComponent<Renderer>();
        initialColor = renderer.material.color;
    }

    [PunRPC]
    public override void RPCInteract(int characterId)
    {
        Event.Invoke();
    }

    private void Update()
    {        
        if (highlight)
        {
            Highlight();
        }
        else if (resetColor)
        {
            ResetColor();
        }
        highlight.Update();
    }

    public virtual void Highlight()
    {
        Highlight(1.2f);
    }

    public virtual void Highlight(float multiplier)
    {
        if (!resetColor)
            initialColor = renderer.material.color;
        Color newColor = initialColor * multiplier;
        newColor.a = initialColor.a;
        renderer.material.color = newColor;
        resetColor = true;
    }
    public void ResetColor()
    {
        renderer.material.color = initialColor;
        resetColor = false;
    }
}
