﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomBar : MonoBehaviour {

    public RoomInfo roomInfo;
    public Text roomIndexText;
    public Text passwordProtectedText;
    public Text roomNameText;
    public Text mapText;
    public Text difficultyText;
    public Text roomStatusText;
    public Text playerCountText;
    public Text pingText;    

    public void AssignValues(int roomIndex, RoomInfo roomInfo)
    {
        this.roomInfo = roomInfo;
        passwordProtectedText.text = "";
        
        if (MultiplayerManager.RoomIsPasswordProtected(roomInfo))        
            passwordProtectedText.text = "P";

        roomIndexText.text = (roomIndex+1).ToString();        
        roomNameText.text = roomInfo.Name;
        mapText.text = System.Enum.GetName(typeof(Level),(Level)GetCustomProperty(CustomPropertyType.Map));
        difficultyText.text = System.Enum.GetName(typeof(Difficulty), (Difficulty)GetCustomProperty(CustomPropertyType.Difficulty));
        if ((bool)GetCustomProperty(CustomPropertyType.InGame)|| (bool)GetCustomProperty(CustomPropertyType.Starting))
            roomStatusText.text = "In Progress";
        else
            roomStatusText.text = "In Lobby";
        //pingText.text = "";
        playerCountText.text = roomInfo.PlayerCount.ToString() + "/" + roomInfo.MaxPlayers.ToString();        
    }        

    public object GetCustomProperty(CustomPropertyType type)
    {
        return roomInfo.CustomProperties[GlobalManager.GameDef.GetCustomProperty(type)];
    }
}
