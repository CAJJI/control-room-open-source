﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour {

    public float amount = 2;
    bool flickerSet;

	// Use this for initialization
	void Start () {
        MenuManager.Instance.AddFlicker(gameObject, amount);
        flickerSet = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!flickerSet)
        {
            MenuManager.Instance.AddFlicker(gameObject, amount);
            flickerSet = true;
        }
	}
}
