﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStatBarUI : WorldButton {

    public GameObject healthBar;
    public SpriteRenderer logo;
    public Sprite engineerLogo;
    public Character character;
    public TextMesh username;

    private void Start()
    {
        Event.AddListener(delegate { CRInterface.Instance.SelectCamera(character.mountedCam); });
        if (character.GetComponent<Engineer>())
            logo.sprite = engineerLogo;
    }

    void Update()
    {
        HealthBar();
    }

    void HealthBar()
    {
        if (character == null || character.health <= 0)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 scale = Vector3.one;
        scale.x = character.health / character.maxHealth;
        healthBar.transform.localScale = scale;        
    }
}
