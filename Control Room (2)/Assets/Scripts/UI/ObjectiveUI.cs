﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveUI : Singleton<ObjectiveUI> {

    public Text text;
    string textString;
    char[] charArray;
    int index;

    private void Update()
    {
        if (charArray == null || index >= charArray.Length) return;
        if (charArray[index] == '\'')
        {
            text.text += charArray[index];
            index++;
            text.text += charArray[index];
        }
        else
        {
            text.text += charArray[index];
        }
        index++;
    }

    public static string UpdateText()
    {
        if (!Instance)
        {
            Debug.Log("Missing objective UI");
            return null;
        }
        return Instance.UpdateTextInstance();
    }

    public string UpdateTextInstance()
    {
        text.text = "";
        index = 0;

        string updatedText;
        updatedText = "LOCKDOWN INITIATED";
        updatedText += AddLine("");
        if (CRInterface.Instance.transmissionDown)
            updatedText += AddLine("REPAIR TRANSMISSION");
        else if (MultiplayerManager.rescueArrived)
            updatedText += AddLine("EXIT FACILITY");
        else
            updatedText += AddLine("AWAIT RESCUE");
        updatedText += AddLine("");
        if (!GlobalManager.cureGenerated)
        updatedText += AddLine("CREATE CURE");
        AssignTextInstance(updatedText);
        return updatedText;
        //charArray = updatedText.ToCharArray();
    }

    public static void AssignText(string text)
    {
        Instance.AssignTextInstance(text);
    }

    public void AssignTextInstance(string text)
    {
        this.text.text = "";
        index = 0;
        textString = text;
        charArray = textString.ToCharArray();
    }

    public string AddLine(string text)
    {
        return "\n" + text;
    }
}
