﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour {

    public Camera cam;
    public Shader shader;

    public float frameDelay = 1;
    float currentFrame = 0;
	
	void Start () {
        cam = GetComponent<Camera>();

        cam.SetReplacementShader(shader, "RenderType");
	}
	
	
	void Update () {        
        currentFrame++;
        if (currentFrame >= frameDelay) {
            cam.enabled = true;
            currentFrame = 0;
        } else {
            cam.enabled = false;
        }
	}
}
