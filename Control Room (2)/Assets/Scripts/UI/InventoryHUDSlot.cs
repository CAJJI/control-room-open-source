﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryHUDSlot : MonoBehaviour {

    public Image slotImage;
    public Image itemImage;
    public Item item;

    public void Select()
    {
        SetAlpha(1);
        if (itemImage.sprite)
            itemImage.gameObject.SetActive(true);
        else
            itemImage.gameObject.SetActive(false);
    }

    public void Unselect()
    {
        SetAlpha(0.5f);
        if (itemImage.sprite)
            itemImage.gameObject.SetActive(true);
        else
            itemImage.gameObject.SetActive(false);
    }

    public void SetAlpha(float value)
    {
        Color color = Color.white;
        color.a = value;
        slotImage.color = color;
        itemImage.color = color;
    }
}
