﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerList : MonoBehaviour {

    public bool active;
    public GameObject backing;

    public Text text;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            SetActive(true);
        }
        else if (active && !Input.GetKey(KeyCode.Tab))
        {
            SetActive(false);
        }

        if (!active) return;
        DisplayPlayers();
    }

    void DisplayPlayers()
    {
        string list = "";
        foreach(Player player in MultiplayerManager.Instance.players)
        {            
            if (PhotonNetwork.inRoom)
            {
                if (player == null || player.photonView.owner == null) continue;
                if (player.dead) list += "(DEAD) ";
                list += player.photonView.owner.NickName;
                list += "\n";
            }
            else
                list += player.name;
        }
        text.text = list;
    }

    public void SetActive(bool state)
    {
        active = state;
        backing.SetActive(state);        
    }
}
